<?php
/*! Include @file class.DBConnManager.php for DBConnManager class */
require_once "../classes/class.DBConnManager.php";
require_once "config.php";



function addLocation($aLocation){

	$DBConn = new DBConnManager();
	$conn = $DBConn->getConnInstance();

	$sDateTime = date("Y-m-d H:i:s");

	$sQuery = "INSERT INTO `location_details`(`id`, `location_name`, `created_by`, `created_date`, `updated_by`, `updated_date`, `is_valid`) VALUES (NULL,'{$aLocation['location_name']}','{$aLocation['created_by']}','{$sDateTime}','{$aLocation['updated_by']}','{$sDateTime}',1)";
	
    $rResult = $conn->query($sQuery);
    	if($rResult){
    		// $iLocationId = $conn->insert_id;
    		return true;
    	}else{
    		return false;
    	}

}


function getAllLocation(){
	$DBConn = new DBConnManager();
	$conn = $DBConn->getConnInstance();

	$sQuery = "SELECT * FROM location_details";

	$rResult = $conn->query($sQuery);

	$aData =  array();
	if($rResult){
		while ($aRow = $rResult->fetch_array()) {
				$aData[] = $aRow;
		}
		return $aData;
	}else{
		return false;
	}
}





?>

