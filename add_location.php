<?php
include('base-header.php');
include('page-header.php');
?>
        <div class="classTopHeading">
        <div class="container">
            <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Welcome Admin</h1>
            </div>
        </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>
        <div id="page-wrappera" class="container">
            
            <!-- /.row -->
            <div class="row">
                <form action="" class="classFormAddLocation" id="idAddLocation" method="post"> 
                    <div class="col-lg-6 col-lg-offset-4">
                         <input type="text" name="addLocation" id="idAddLocation" class="form-control form-group classAddLocation">
                    </div>  
                      
                    <div class="col-lg-2">
                        <input type="button" class="btn btn-md btn-primary classAddButton" id="idAddLocationBtn" value="Add Location">
                    </div>
                </form>    
            </div>

            <div class="row">
                <div class="col-lg-6 col-lg-offset-4">
                    <section>
                    <table class="table table-bordered " id="idLocationTable" >
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Location</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody id="idTableLocation">
                            </tbody>
                    </table>
                   </section> 

                   <section>
            <h1>Editor example <span>Simple inline editing</span></h1>

            <table id="example" class="display" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>

                        <th>First name</th>
                        <th>Last name</th>
                        <th>Position</th>
                        <th>Office</th>
                        <th width="18%">Start date</th>
                        <th>Salary</th>
                    </tr>
                </thead>
            </table>
                </section>
                </div>                        
            </div> 
          
        </div>
        <!-- /#page-wrapper -->


<?php
include('page-footer.php');
include('base-footer.php');
?>

<script type="text/javascript">
    // getAllLocation();
    $("#idAddLocationBtn").click(function(event){
           var formData = $("#idAddLocation").serializeArray();
           $.ajax({
                method: "post",
                url: "ajax_location.php?func=0",
                data: formData,
                success: function(data){
                    if(data){
                        alert("Location Added Successfully");
                    }else{
                        alert("Location not added");
                    }
                }
           });

    });

    // function getAllLocation(){
    //     $.ajax({
    //         url: "ajax_location.php?func=1",
    //         method: "post",
    //         dataType: "json",
    //         success: function(data){
    //             var aLocation = data;
    //             var iLength = aLocation.length;
    //             var sTemplate = "";
    //             var jjj = 1;
    //             for(var iii=0; iii<iLength; iii++){
    //                 sTemplate += "<tr>";
    //                 sTemplate += "<td>"+jjj+"</td>";
    //                 sTemplate += "<td>"+aLocation[iii]['location_name']+"</td>";
    //                 sTemplate += "<td></td>";
    //                 sTemplate += "</tr>";
    //                 jjj++;
    //             }
    //             $("#idTableLocation").html(sTemplate);
    //         } 
    //     });
    // }
var editor; // use a global for the submit and return data rendering in the idLocationTables

$(document).ready(function() {
    editor = new $.fn.dataTable.Editor( {
        ajax: "../php/staff.php",
        table: "#idLocationTable",
        fields: [ {
                label: "First name:",
                name: "first_name"
            }, {
                label: "Last name:",
                name: "last_name"
            }, {
                label: "Position:",
                name: "position"
            }, {
                label: "Office:",
                name: "office"
            }, {
                label: "Extension:",
                name: "extn"
            }, {
                label: "Start date:",
                name: "start_date",
                type: "date"
            }, {
                label: "Salary:",
                name: "salary"
            }
        ]
    } );

    // Activate an inline edit on click of a table cell
    $('#idLocationTable').on( 'click', 'tbody td:not(:first-child)', function (e) {
        editor.inline( this );
    } );

    $('#idLocationTable').DataTable( {
        dom: "Tfrtip",
        ajax: "../php/staff.php",
        columns: [
            { data: null, defaultContent: '', orderable: false },
            { data: "first_name" },
            { data: "last_name" },
            { data: "position" },
            { data: "office" },
            { data: "start_date" },
            { data: "salary", render: $.fn.dataTable.render.number( ',', '.', 0, '$' ) }
        ],
        order: [ 1, 'asc' ],
        tableTools: {
            sRowSelect: "os",
            sRowSelector: 'td:first-child',
            aButtons: [
                { sExtends: "editor_create", editor: editor },
                { sExtends: "editor_edit",   editor: editor },
                { sExtends: "editor_remove", editor: editor }
            ]
        }
    } );
} );


</script>