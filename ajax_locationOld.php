<?php
	require("../classes/class.DBConnManager.php");
	require "function_library.php";



	$aFunctionMap = array(  0 => "ajax_addLocation",
							1 => "ajax_getAllLocation"

	
	);

$iFunc = $_REQUEST['func'];
if(isset($aFunctionMap[$iFunc])) {
	$method = $aFunctionMap[$iFunc];

	$aResponse = $method();

	header("Content-Type: application/json");
	echo json_encode($aResponse);
}
else {
	header("HTTP/1.1 401 Unauthorized");
	exit;
}


function ajax_addLocation(){
			$DBconn = new DBConnManager();
			$conn = $DBconn->getConnInstance();
			
			$sLcationName = addslashes($_POST['addLocation']);
			
			$aLocation = array();
			$aLocation['location_name'] = $sLcationName;
			$aLocation['created_by'] = 1;
			$aLocation['updated_by'] = 1;
			$bResponse = addLocation($aLocation);
			return $bResponse;
	}	


	function ajax_getAllLocation(){
			$DBconn = new DBConnManager();
			$conn = $DBconn->getConnInstance();

			$aLocation = array();

			$bResponse = getAllLocation();
			return $bResponse;
	}
?>