</div>
    <!-- /#wrapper -->
<div class="classFooterWrapper">
<div class="container">
 <footer>
    <div class="row">
    	<div class="col-lg-3">
			<div class="cuadro_intro_hover " style="background-color:#cccccc;">
				<p style="text-align:center; margin-top:20px;">
					<img src="images/before.jpg" class="img-responsive" alt="">
				</p>
				<div class="caption">
					<div class="blur"></div>
					<div class="caption-text">
						<h3 style="border-top:2px solid white; border-bottom:2px solid white; padding:10px;">Checkup Request</h3>
						<p>Loren ipsum dolor si amet ipsum dolor si amet ipsum dolor</p>
						<a class=" btn btn-default" href="#"><span class="glyphicon glyphicon-plus"> INFO</span></a>
					</div>
				</div>
			</div>				
	    </div>
       <div class="col-lg-3">
			<div class="cuadro_intro_hover " style="background-color:#cccccc;">
				<p style="text-align:center; margin-top:20px;">
					<img src="images/after.jpg" class="img-responsive" alt="">
				</p>
				<div class="caption">
					<div class="blur"></div>
					<div class="caption-text">
						<h3 style="border-top:2px solid white; border-bottom:2px solid white; padding:10px;">Checkup Report</h3>
						<p>Loren ipsum dolor si amet ipsum dolor si amet ipsum dolor</p>
						<a class=" btn btn-default" href="#"><span class="glyphicon glyphicon-plus"> INFO</span></a>
					</div>
				</div>
			</div>				
	    </div>
		<div class="col-lg-3 col-md-6">
			<h3>Categories:</h3>
		    <ul>
			<li><a href=""><i class="fa fa-flask">Test name</i></a></li>
			<li><a href=""><i class="fa fa-flask">Test name</i></a></li>
			<li><a href=""><i class="fa fa-flask">Test name</i></a></li>
			<li><a href=""><i class="fa fa-flask">Test name</i></a></li>
			<li><a href=""><i class="fa fa-flask">Test name</i></a></li>
			<li><a href=""><i class="fa fa-flask">Test name</i></a></li>
			<li><a href=""><i class="fa fa-flask">Test name</i></a></li>
			</ul>
		</div>
        <div class="col-lg-3 col-md-6">
            <h3>Contact:</h3>
			<p>Have a question or feedback? Contact me!</p>
			<p><a href="" title="Contact me!"><i class="fa fa-envelope"></i> Contact</a></p>
        	<h3>Follow:</h3>
			<a href="" id="gh" target="_blank" title="Twitter">
				<span class="fa-stack fa-lg">
				  <i class="fa fa-square-o fa-stack-2x"></i>
				  <i class="fa fa-twitter fa-stack-1x"></i>
			</span>
			Twitter</a>
			<a href="" id="gh" target="_blank" title="Twitter">
				<span class="fa-stack fa-lg">
				  <i class="fa fa-square-o fa-stack-2x"></i>
				  <i class="fa fa-facebook fa-stack-1x"></i>
			</span>
			Facebook</a>
			
		</div>
		<br/>
    </footer>    
</div>
</div>    