<?php
include_once "class.DBConnManager.php";
include_once "class.SessionManager.php";

/**
* @brief This class represents a EmailTemplate and it's behavior.
*/
class EmailTemplate{
	
	//! $sTemplateName holds the Email Template Name..
	public $sTemplateName;

	//! $sTemplateSubject holds the Template Subject..
	public $sTemplateSubject;

	//! $sTemplateText holds the Template body..
	public $sTemplateText;
	
	//! $iOrgId holds the Organisation Id..
	public $iOrgId;	
	
	/* @brief initialize the EmailTemplate class..
	** @param $aETData(email template data) array which hold Email Template information..
	*/
	function __construct($aETData)	{

		//! check $aETData array is empty or not ...
		if(!empty($aETData))
		{
			//! check Template name is empty or not... 	
			if(!empty($aETData['templateName'])){
				$this->sTemplateName = $aETData['templateName'];
			}
			else{
				$this->sTemplateName = NULL;
			}	

			//! check Template subject is empty or not... 	
			if(!empty($aETData['templateSubject'])){
				$this->sTemplateSubject = $aETData['templateSubject'];
			}
			else{
				$this->sTemplateSubject = NULL;
			}	

			//! check Template text is empty or not... 	
			if(!empty($aETData['templateBody'])){
				$this->sTemplateText = $aETData['templateBody'];
			}
			else{
				$this->sTemplateText = NULL;
			}

			//! check Organisation id is empty or not... 	
			if(!empty($aETData['orgId'])){
				$this->iOrgId = $aETData['orgId'];
			}
			else{
				$this->iOrgId = NULL;
			}			
		}
		else			
		{
			return false;
		}
		
	}


	/*! @brief create the Email Template
    *  Calling this function will create the email template. Once we have set the template details, we can call this function and add its entry in the database.
    * @return $iETId 
    */
    function createEmailTemplate(){
        $sessionManager = new SessionManager();
        $DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();
        $sDate = date('Y-m-d');
		$iUserId = $sessionManager->iUserID;

        $sTableName = "emailtemplate";
        $sQuery = "INSERT INTO {$sTableName} (`emailTemplateId`, `id_fs_templateName`, `id_fs_templateSubject`, `id_fs_templateText`, `orgId`, `addedOn`, `addedBy`, `deleted`) VALUES (Null,'{$this->sTemplateName}','{$this->sTemplateSubject}','{$this->sTemplateText}','{$this->iOrgId}','{$sDate}','{$iUserId}',0)";
       //var_dump($sQuery);exit();
        $rResult = $conn->query($sQuery);

        if($rResult) {
            $iETId = $conn->insert_id;         
            return $iETId;
        }
        else {            
            return false;
        }
    }    


    /*! @Brief get the Email Template Details..
	* @param $iETId
	* @return $aETData
    */
    function getEmailTemplateDetails($iETId){
    	$DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();        

        $sQuery = "SELECT * FROM `emailtemplate` WHERE `emailTemplateId` = {$iETId} and `deleted` = 0";        
        $result = $conn->query($sQuery);

		$aETData = array();    
		if($result!==FALSE){
			while($row = $result->fetch_array()) {
				$aETData = $row;
			}
		}
		else{		
			return false;
		}
		return $aETData;
    }  

    /*! @Brief get the all Email Template Details..
	* @return $aETData as array.
    */
    function getAllEmailTemplateDetails($iOrgId){
    	$DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();        

        $sQuery = "SELECT * FROM `emailtemplate` WHERE `orgId`= '{$iOrgId}' and`deleted` = 0";        
        $result = $conn->query($sQuery);

		$aETData = array();    
		if($result!==FALSE){
			while($row = $result->fetch_array()) {
				$aETData[] = $row;
			}
		}
		else{		
			return false;
		}
		return $aETData;
    } 


    /*! @brief update the Email Template..
    * @param $iETId, unique email template id..
  	* @return $iETId..
    */
    function updateEmailTemplate($iETId){
        $sessionManager = new SessionManager();
        $DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();
        $sDate = date('Y-m-d');
		$iUserId = $sessionManager->iUserID;

        $sTableName = "emailtemplate";
        $sQuery = "UPDATE `emailtemplate` SET `deleted`= 1 WHERE `emailTemplateId` = '{$iETId}'";
        $rResult = $conn->query($sQuery);

        if($rResult) {
            $sQuery = "INSERT INTO {$sTableName} (`emailTemplateId`, `id_fs_templateName`, `id_fs_templateSubject`, `id_fs_templateText`, `orgId`, `addedOn`, `addedBy`, `deleted`) VALUES (Null,'{$this->sTemplateName}','{$this->sTemplateSubject}','{$this->sTemplateText}','{$this->iOrgId}','{$sDate}','{$iUserId}',0)";
	        $rResult = $conn->query($sQuery);

	        if($rResult) {
	            $iETId = $conn->insert_id;         
	            return $iETId;
	        }
        }
        else {            
            return false;
        }
    }

    /*! @brief to update subject of Email Template..
    * @param $sUpdatedSubject, $iETId, unique email template id..
    */
	public function updateSubject($sUpdatedSubject,$iETId){
		$DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();
		
		$sTableName = "emailtemplate";
        $sQuery = "UPDATE `emailtemplate` SET `id_fd_templateSubject`= '{$sUpdatedSubject}' WHERE `emailTemplateId` = '{$iETId}'";
        $rResult = $conn->query($sQuery);
        if($rResult) {
        	return true;
        }
	}


	/*! @brief to update Text of Email Template..
    * @param $sUpdatedText, $iETId, unique email template id..
    */
	public function updateText($sUpdatedText,$iETId){
		$DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();
		
		$sTableName = "emailtemplate";
        $sQuery = "UPDATE `emailtemplate` SET `id_fd_templateText`= '{$sUpdatedText}' WHERE `emailTemplateId` = '{$iETId}'";
        $rResult = $conn->query($sQuery);
        if($rResult) {
        	return true;
        }
	}


    /*! @brief delete the Email Template..
    * @param $iETId, unique Email Template id..
  	* @return true on success, otherwise false..
    */
    function deleteEmailTemplate($iETId){
        $DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();       

        $sTableName = "emailtemplate";
        $sQuery = "UPDATE `emailtemplate` SET `deleted`= 1 WHERE `emailTemplateId` = '{$iETId}'";
        $rResult = $conn->query($sQuery);
        if($rResult) {
            return true;
        }
        else {            
            return false;
        }
    }

    /*! @Brief get the all Deleted Email Template Details..
	* @return $aDeletedETData as array.
    */
    function getAllDeletedTemplate(){
    	$DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();        

        $sQuery = "SELECT * FROM `emailtemplate` WHERE `deleted` = 1";        
        $result = $conn->query($sQuery);

		$aDeletedETData = array();    
		if($result!==FALSE){
			while($row = $result->fetch_array()) {
				$aDeletedETData[] = $row;
			}
		}
		else{		
			return false;
		}
		return $aDeletedETData;
    } 

	
	/* 
	** for destruct the EmailTemplate class.
	*/
	function __destruct(){
		//echo "Class Destroyed";
	}		
}

?>