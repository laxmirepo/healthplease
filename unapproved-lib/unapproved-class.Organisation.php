<?php
include_once "class.DBConnManager.php";
/**
* @brief This class represents a Organisation and it's behavior.
*/
class Organisation{
	
	//! $sOrgName holds the Organisation Name..
	private $sOrgName;
	
	//! $sOrgType holds the User Defined Organisation Type..
	private $sOrgType;
	
	//! $sRegistrationNo holds the organisation registration number..
	private $sOrgRegistrationNo;

/*
	private $sOrgVATNo;

	private $sOrgTaxNo;

	private $sOrgTINNo;

	private $sOrgCSTNo;
*/

	//! Associative array of Various Financial and Tax Nos/Codes
	//! E.g PAN, VAT, TIN, CST, etc etc.
	//! E.g $aOrgFinancialNos[] = array('VAT'=>'ADPP978555656');
	private $aOrgFinancialNos = array(); 

	//! $sEstablishDate holds the organisation establish date..
	private $sOrgEstablishDate;

	//! $sOrgAddress holds the organisation address..
	public $aOrgAddresses;

	//! $sOrgDirector array of directorys for the organisation..
	public $aOrgDirector;

	//! $sUserId holds the id of admin..
	public $sUserId;	
	
	//! $aOrgEmployee holds the array of organisation employee..
	private $aOrgEmployee = array();
	

	

	
	/* @brief initialize the Organisation class
	** @param $aOrgData array which hold User information.
	*/
	function __construct($aOrgData)	{

		//! check $aOrgData array is empty or not ...
		if(!empty($aOrgData))
		{
			//! check organisarion name is empty or not... 	
			if(!empty($aOrgData['orgName'])){
					$this->sOrgName = $aOrgData['orgName'];
				}
				else{
					//$this->sOrgName = NULL;
					echo "Organisation name not contains value.";
				}

			//! check organisarion type is empty or not...
			if(!empty($aOrgData['orgType'])){
					$this->sOrgType = $aOrgData['orgType'];
				}
				else{
					//$this->sOrgType = NULL;
					echo "Organisation Type contains value.";
				}

			//! check organisarion registration number is empty or not...
			if(!empty($aOrgData['registrationNo'])){
					$this->sOrgRegistrationNo = $aOrgData['registrationNo'];
				}
				else{
					//$this->sOrgRegistrationNo = NULL;
					echo "Organisation registration no. not contains value.";
				}

			//! check organisarion financial no.  is empty or not...
			if(!empty($aOrgData['orgFinancialNos'])){
					$this->$aOrgFinancialNos = $aOrgData['orgFinancialNos'];
				}
				else{
					$this->sOrgEstablishDate = NULL;
					//echo "Organisation financial no. not contains value.";
				}

			//! check organisarion establish date  is empty or not...
			if(!empty($aOrgData['establishDate'])){
					$this->sOrgEstablishDate = $aOrgData['establishDate'];
				}
				else{
					//$this->sOrgEstablishDate = NULL;
					echo "Organisation Establish date not contains value.";
				}

			

			//! check organisarion Director Name is empty or not...
			if(!empty($aOrgData['orgDirector']))	{
					$this->aOrgDirector = $aOrgData['orgDirector'];
				}
				else{
					//$this->aOrgDirector = NULL;
					echo "Organisation Director not contains value.";
				}

			//! check organisarion address is empty or not...
			if(!empty($aOrgData['orgAddress'])){
					$this->aOrgAddresses = $aOrgData['orgAddress'];
				}
				else{
					$this->aOrgAddresses = NULL;
				}

							
			//! check organisarion employee is empty or not...												
			if(!empty($aOrgData['orgEmp'])){		
						$this->aOrgEmployee =  $aOrgData['orgEmp']; 
				}
				else{
					$this->aOrgEmployee = NULL;
				}	

			//! check Contact empty or not...												
			if(!empty($aOrgData['orgContact'])){		
						$this->aOrgContact =  $aOrgData['orgContact']; 
				}
				else{
					$this->aOrgContact = NULL;
				}

			//! check organisarion user(Admin) is empty or not...												
			if(!empty($aOrgData['userId'])){		
						$this->sUserId =  $aOrgData['userId']; 
				}
				else{
					$this->sUserId = NULL;
				}					
		}
		else			
		{
			return false;
		}
		
	}

	/*! @brief add the Organisation details  
    * @param $aOrgData as array...
    * @return $iOrgId...
    */
	function addOrganisation($aOrgData){
	//! for getting database connection..
		$DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();
        //! query for inserting organisation entry in database..
        $sTableName = "organisation";
		$sQuery = "INSERT INTO {$sTableName}(`orgid`, `id_fs_orgName`, `id_fs_orgType`, `id_fs_orgRegistrationNo`, `id_fs_orgFinancialNos`, `id_fs_orgEstablishDate`, `id_fs_orgAddresses`, `id_fs_orgDirector`) VALUES (Null,'{$aOrgData['orgName']}','{$aOrgData['orgType']}','{$aOrgData['registrationNo']}','{$aOrgData['orgFinancialNos']}','{$aOrgData['establishDate']}','{$aOrgData['orgAddress']}','{$aOrgData['orgDirector']}')";
        $rResult = $conn->query($sQuery);

        if($rResult) {
            $this->iOrgId = $conn->insert_id;
        	$sQuery = "INSERT INTO `adminorgmapping`(`AOMId`, `orgid`, `user_id`, `isAdmin`, `deleted`) VALUES (Null,'{$this->iOrgId}','{$aOrgData['userId']}',1,0)";
       		$rResult = $conn->query($sQuery);
            return $this->iOrgId;
        }
        else {            
            return false;
        }		
	}



	/*! @brief get the Organisation details  
    * @return $aOrgData as array...
    */
    public function fGetOrganisationDetails() {
        
        $DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();
        
        $sTableName = "organisation";

        $sQuery = "SELECT * FROM {$sTableName}";
        
        $result = $conn->query($sQuery);

		$aOrgData = array();    
		if($result!==FALSE){

			while($row = $result->fetch_array()) {
				$aOrgData[] = $row;
			}

		}
		else{		
			return false;
		}
		return $aOrgData;
    }

    /*! @brief get the Organisation Id 
    * @return $iOrgId as ..
    */
    public function getOrgIdByUserId($iUserId) {
        
        $DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();
        
        $sTableName = "adminorgmapping";

        $sQuery = "SELECT orgid FROM {$sTableName} WHERE `user_id` = {$iUserId}";
        
        $result = $conn->query($sQuery);

		$iOrgId = array();    
		if($result!==FALSE){

			while($row = $result->fetch_array()) {
				$iOrgId = $row[0];
			}

		}
		else{		
			return false;
		}
		return $iOrgId;
    }

    /*! @brief get the Organisation details 
    * @param $iOrgId which is unique organisation id..
    * @return $aOrgData..
    */
    public function getOrgDetails($iOrgId) {
        
        $DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();
        
        $sTableName = "organisation";

        $sQuery = "SELECT * FROM {$sTableName} WHERE `orgid` = {$iOrgId}";
        
        $result = $conn->query($sQuery);

		$aOrgData = array();    
		if($result!==FALSE){

			while($row = $result->fetch_array()) {
				$aOrgData = $row;
			}

		}
		else{		
			return false;
		}
		return $aOrgData;
    }


    /*! @brief check the organisation is available or not for particular user id
     * @param $iUserId which is unique id of user..
    * @return 1 if yes, 0 if no.
    */
    public function isOrganisation($iUserId) {
        
        $DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();
        
        $sTableName = "adminorgmapping";

        $sQuery = "SELECT orgid FROM {$sTableName} WHERE `user_id` = {$iUserId} and `orgid` != 0";
        $result = $conn->query($sQuery);
		if($result!==FALSE){
			while($row = $result->fetch_array()) {
				$bIsOrg = $row[0];
			}
			//var_dump($bIsOrg);
			if($bIsOrg == NULL){				
				return 0;
			}
			else{		
				return 1;
			}
		}
		else{		
			return 2;
		}
    }


    /*! @brief get the all Organisations which is store in database.. 
    * @return $aOrgData as array ..
    */
    public function getAllOrganisation() {
        
        $DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();
        
        $sTableName = "organisation";

        $sQuery = "SELECT * FROM {$sTableName}";
        
        $result = $conn->query($sQuery);

		$aOrgData = array();    
		if($result!==FALSE){

			while($row = $result->fetch_array()) {
				$aOrgData[] = $row;
			}

		}
		else{		
			return false;
		}
		return $aOrgData;
    }



    	
	/* 
	** for destruct the Organisation class.
	*/
	function __destruct()
		{
			//echo "Class Destroyed";
		}		
}

?>