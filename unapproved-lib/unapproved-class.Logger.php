<?php
/*!
 * @class Logger
 * This class is used for logging.
 */
global $_oLogger;
$_oLogger = new Logger(dirname(__FILE__).DIRECTORY_SEPARATOR."log.txt", Logger::DEBUG);
class Logger {
	//! Path to the file for logging
	public $sLogFilePath;

	//! Threshold for error logging. Event is only logged if it is under threshold
	public $iLogThreshold;

	//! date format to be used in the log file
	public $dateFormat = 'Y-m-d H:i:s';

	//! Defining different log level contstants
	const EMERGENCY = 0;
    const ALERT     = 1;
    const CRITICAL  = 2;
    const ERROR     = 3;
    const WARNING   = 4;
    const NOTICE    = 5;
    const INFO      = 6;
    const DEBUG     = 7;

    /*! @brief initialize the Logger class
    * @param $sLogFilePath Path to the file to be used for logging
    * @param $iLogThreshold Logging threshold value
    * @note Only log events that are under threshold will be logged. Events above the threshold will be 
    *       ignored
    *
    */
    function __construct($sLogFilePath, $iLogThreshold = Logger::DEBUG) {
    	$this->sLogFilePath = $sLogFilePath;
    	$this->iLogThreshold = $iLogThreshold;
    }

    private function log($sLevel, $sMeesage, $aData = null) {

    	$sLevel = strtoupper($sLevel);

    	//! Get the value of the logging from method called
    	$iLevel = constant("self::{$sLevel}");


        //! Use  index 1 because we are calling this function from this call again using magic method
        $aDebugTrace = debug_backtrace();

    	//! Log only if under the threshold
    	if($iLevel <= $this->iLogThreshold) {

    		$sTimeStamp = $this->getTimestamp();

    		$sData = "[{$sTimeStamp}][{$sLevel}]: {$sMeesage} at {$aDebugTrace[1]['file']} on line {$aDebugTrace[1]['line']}";

    		//! If data is supplied, convert it to a readable format and add along with data.
    		if($aData!= null) {
    			$sData .= PHP_EOL.print_r($aData, true).PHP_EOL;
    		}
    		$sData .= PHP_EOL;

    		//! Use php's error log function to write to the file
    		error_log($sData, 3, $this->sLogFilePath);
    	}
    }

    //! Catch the call for logging
    function __call($method, $arg) {
    	$this->log($method, $arg[0], $arg[1]);
    }

    //! generate appropriate timestamp
    private function getTimestamp()
    {
        $originalTime = microtime(true);
        $micro = sprintf("%06d", ($originalTime - floor($originalTime)) * 1000000);
        $date = new DateTime(date('Y-m-d H:i:s.'.$micro, $originalTime));

        return $date->format($this->dateFormat);
    }

    static public function getInstance() {
        global $_oLogger;
        return $_oLogger;
    }
}
