<?php
/*! Include @file class.DBConnManager.php for DBConnManager class */
require_once "class.DBConnManager.php";
require_once "class.User.php";
require_once "class.PermissionHandler.php";


/*!
 * @class SessionManager
 * This class manages the user session.
 */

class SessionManager {
    public $isLoggedIn;
    public $iUserID;
    public $sUsername;
    public $sName;
    public $sError;
    public $iType;
    public $sLoginDate;
    public $sLoginTime;
    public $sIP;
    public $sBrowser;
    public $sDeviceType;
    public $lastActivity;
    public $iLoginInstanceID;
    public $autoLoginSession;
    public $aPermMatrix;

    //! constructor
    function __construct()
    {
        session_start();
        if($_SESSION['_0000007_lastActivity'] < (time() - 30*60)){
            if ($this->autoLogin()) {
            }
            else {
                $this->logOut();
            }
            return;
        }
        if($_SESSION['_0000007_isLoggedIn'] === TRUE){
            $this->isLoggedIn = TRUE;
            $this->iUserID=$_SESSION['_0000007_iUserID'];
            $this->sUsername=$_SESSION['_0000007_sUsername'];
            $this->sName=$_SESSION['_0000007_sName'];
            $this->sLoginDate=$_SESSION['_0000007_sLoginDate'];
            $this->sLoginTime=$_SESSION['_0000007_sLoginTime'];
            $this->iType=$_SESSION['_0000007_iType'];
            $this->sIP = $_SERVER['REMOTE_ADDR'];
            $this->sBrowser = $_SERVER['HTTP_USER_AGENT'];
            $this->sDeviceType = "undefined";
            $_SESSION['_0000007_lastActivity'] = time();
            $this->lastActivity = time();
            $this->sRememberMe = $_SESSION['_0000007_RememberMe'];
            $this->sError = NULL;        
            $this->autoLoginSession = $_SESSION['_0000007_autoLoginSession'];


            $this->aPermMatrix = unserialize(urldecode($_SESSION['_0000007_permMatrix']));
        }
        else {
            $this->isLoggedIn = FALSE;
        }

    }
    
    //! @return boolean Boolean value stating whether current user is logged in or not
    function isLoggedIn()
    {
        return $this->isLoggedIn;

    }

    /*!
    * @brief Redirects User to login page if not logged in 
    * This function should be called on the top of the pages which require user to be authenticated.
    * If user is not authenticated, it will be redirected to login page. On successful login, it will be
    * redirected to url specificed in argument
    */
    function requireAuth($sURL = FALSE)
    {
        if(!$this->isLoggedIn()){
            header('Location: index.php');
            exit;
        }
    }

    /*!
    * @brief Does not allow user to view page if he doesn't have required level
    * This function should be called on the top of the pages which require particular level.
    * If user does not have required level, he will be redirected to dashboard with error message.
    * @param $iLevel integer the maximum level integer that can access this page.
    */
    function requireLevel($iLevel){
        if(!$this->isLoggedIn()){
            $sMsg = array();
            $sMsg[] = "E13";
            //! Redirect User with appropriate alert message
		redirectWithAlert("index.php", $sMsg);
            return;
        }
        
        if($this->iType > $iLevel){
            $sMsg = array();
            $sMsg[] = "E12";
            //! Redirect User with appropriate alert message
		redirectWithAlert("account.php", $sMsg);
            return;
        }

    }


    //! Authenticates the users 
    //! @return boolean TRUE if login was successful, FALSE if failed.
    function authenticateUser($sUsername, $sPassword, $bRemember = FALSE)
    {
        //! trim username to remove leading and trailing whitespaces
        $sUsername = trim($sUsername);
        $sPassword = $sPassword;
        if ($sUsername == '' || $sPassword == '')
        {
            $this->sError = INVALID_DATA;
            return FALSE;
        }
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        //! Senitize the username & password to prevent SQL Injections
        $sUsername = $conn->real_escape_string($sUsername);
        $sPassword = $conn->real_escape_string($sPassword);
        $sLoginTable = 'user_logins';
        $sQuery = "SELECT user_id,password,verified FROM {$sLoginTable} WHERE username='{$sUsername}'";
        $result = $conn->query($sQuery);

        if(!$result){
            $this->sError = DATABASE_ERROR;
            return FALSE;
        }
        $aTemp = $result->fetch_array();
        $sRealPass = $aTemp[1];
        $iUID = $aTemp[0];

        //! verify the user password and check if user has verified his user account. 
        //! Take logical AND for both to meet both requirements
        $verified = $this->verifyPassword($sPassword,$sRealPass) && $aTemp[2];

        //! If verified, login. Set required session fields
        if($verified){

            $this->isLoggedIn = TRUE;
            $_SESSION['_0000007_isLoggedIn']=TRUE;
            $this->iUserID=$iUID;
            $oUser = new User($this->iUserID);
            $this->sUsername=$oUser->sUsername;
            $_SESSION['_0000007_sUsername'] = $this->sUsername;
            $this->iType = $oUser->iType;
            $_SESSION['_0000007_iType'] = $oUser->iType;
            $this->sName=$oUser->sName;
            $_SESSION['_0000007_sName']=$oUser->sName;
            $_SESSION['_0000007_sLoginDate'] = date('Y-m-d');
            $_SESSION['_0000007_iUserID'] = $this->iUserID;
            $_SESSION['_0000007_sLoginTime'] = date('H:i:s',time());
            $this->sLoginDate=$_SESSION['_0000007_sLoginDate'];
            $this->sLoginTime=$_SESSION['_0000007_sLoginTime'];
            $_SESSION['_0000007_lastActivity'] = time();
            $this->sIP = $_SERVER['REMOTE_ADDR'];
            $this->sBrowser = $_SERVER['HTTP_USER_AGENT'];
            $this->sDeviceType = "undefined";
            $this->saveLoginInstance();
            $this->aPermMatrix = PermissionHandler::getPermissionModel($oUser);

            $_SESSION['_0000007_permMatrix'] = urlencode(serialize($this->aPermMatrix));

            if($bRemember){
                $this->storeRememberMeCookie($iUID);
            }
            //loginHook($this->iUserID, false);
            return TRUE;
        }
        else {
            $this->sError = WRONG_CREDENTIAL;
            return FALSE;
        }
    }


    //! Match @param $sPlainPassword and @sPasswordHash using proprietary algorighm that was used to generate the hash
    static function verifyPassword($sPlainPassword, $sPasswordHash)
    {
        //! retrieve the salt
        //$salt = substr($sPasswordHash,0,32);
        //! retrive the actual hash.
        //$hash = substr($sPasswordHash,32);
        //! generate the actual salt for plainpassword for given salt
        //$sSHAHash = hash("sha256", $salt.$sPlainPassword);

        $sha1Password = sha1($sPlainPassword);

/*

        echo $sha1Password;
        echo "<br />";
        echo $sPasswordHash;
        exit();
*/


        if($sPasswordHash == $sha1Password){
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    //! logs out the user
    function logOut(){
        //! Unset the Session variables
        $_SESSION['_0000007_isLoggedIn']=FALSE;
        unset($_SESSION['_0000007_sUsername']);
        unset($_SESSION['_0000007_iType']);
        unset($_SESSION['_0000007_sName']);
        unset($_SESSION['_0000007_sLoginTime']);
        unset($_SESSION['_0000007_autoLoginSession']);

        //unset($_SESSION);
        session_unset(); 
       // session_destroy(); 

        //! Destroy cookies
        setcookie("uid", $iUID, time()-3600);
        setcookie("remembermehash", $remembermehash, time()-3600);

        if(strlen($this->iUserID)>0) {
            //! Audit the AutoLogin
       //     $auditTrail = new AuditTrail();
        //    $auditMsg = "User [user]{$this->iUserID}[/user] was logged out.";
        //    $auditTrail->addAudit(23, $auditMsg);
        }
        // session_destroy(); 
    }

    //! Function that saves the login instance in database
    function saveLoginInstance(){

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sLoginInstanceTable = 'login_instances';
        $sQuery = "INSERT INTO `{$sLoginInstanceTable}` (`id`, `user_id`, `login_date`, `login_time`, `browser`, `ip`, `device_type`) VALUES (NULL, {$this->iUserID}, '{$this->sLoginDate}', '{$this->sLoginTime}', '{$this->sBrowser}', '{$this->sIP}', '{$this->sDeviceType}')";
            
        $conn->query($sQuery);
        $this->iLoginInstanceID = $conn->insert_id;
        $_SESSION['_0000007_iLoginInstanceID']= $this->iLoginInstanceID;

    }

    /*! @fn function getLoggedInUser()
    * @brief Gives the logged in user
    * This function will return the object of User who is currently logged In.
    * @return object It will return the object of User class
    */
    function getLoggedInUser(){
        return new User($this->iUserID);
    }

    //! @brief Stores the Remember Me Cookie
    private function storeRememberMeCookie($iUID){
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sLoginTable = 'web_users';
        $sQuery = "SELECT Password FROM {$sLoginTable} WHERE UserID='{$iUID}'";
        $result = $conn->query($sQuery);
        $aResult = $result->fetch_array();
        
        //! Hash the password hash again!
        $salt = md5(uniqid('', true));
        $sSHAHash = hash("sha256", $salt.$aResult[0]);
        $remembermehash = $salt.$sSHAHash;
        

        //! set cookie for user_id 
        setcookie("uid", $iUID, time()+3600*24*REMEMBER_ME_EXPIRE_TIME);

        //! set cookie for hash
        setcookie("remembermehash", $remembermehash, time()+3600*24*REMEMBER_ME_EXPIRE_TIME);
    }

    //! @brief Auto-logins user if valid Remember Me cookies
    private function autoLogin(){
        if(!(isset($_COOKIE['uid']) && isset($_COOKIE['remembermehash']))){
            return FALSE;
        }
        $uid = $_COOKIE['uid'];
        $remembermehash = $_COOKIE['remembermehash'];
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        
        $sLoginTable = 'web_users';
        $sQuery = "SELECT Password FROM {$sLoginTable} WHERE UserID='{$uid}'";
        $result = $conn->query($sQuery);
        $aResult = $result->fetch_array();

        $verified = $this->verifyPassword($aResult[0],$remembermehash);
        if($verified){
            $this->isLoggedIn = TRUE;
            $_SESSION['_0000007_isLoggedIn']=TRUE;
            $this->iUserID=$uid;
            $oUser = new User($this->iUserID);
            $this->sUsername=$oUser->sUsername;
            $_SESSION['_0000007_sUsername'] = $this->sUsername;
            $this->iType = $oUser->iType;
            $_SESSION['_0000007_iType'] = $oUser->iType;
            $this->sName=$oUser->sName;
            $_SESSION['_0000007_sName']=$oUser->sName;
            $_SESSION['_0000007_sLoginDate'] = date('Y-m-d');
            $_SESSION['_0000007_iUserID'] = $this->iUserID;
            $_SESSION['_0000007_sLoginTime'] = date('H:i:s',time());
            $this->sLoginDate=$_SESSION['_0000007_sLoginDate'];
            $this->sLoginTime=$_SESSION['_0000007_sLoginTime'];
            $_SESSION['_0000007_lastActivity'] = time();
            $this->sIP = $_SERVER['REMOTE_ADDR'];
            $this->sBrowser = $_SERVER['HTTP_USER_AGENT'];
            $this->sDeviceType = "undefined";
            $this->saveLoginInstance();

            $this->autoLoginSession = TRUE;
            $_SESSION['_0000007_autoLoginSession'] = TRUE;

            //! Audit the AutoLogin
          //  $auditTrail = new AuditTrail();
          //  $auditMsg = "User [user]{$this->iUserID}[/user] was automatically logged in.";
          //  $auditTrail->addAudit(1, $auditMsg);

            loginHook($this->iUserID, true);
            return TRUE;
        }
        else {

            //! Delete Cookies inorder to avoid redirect loop
            setcookie("uid", $iUID, time()-3600*24);
            setcookie("remembermehash", $remembermehash, time()-3600*24);
            return FALSE;
        }
    }
}

?>