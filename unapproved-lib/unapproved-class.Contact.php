<?php
include_once "class.Person.php";
include_once "class.DBConnManager.php";
include_once "class.SessionManager.php";

$sessionManager = new SessionManager();


/**
*  @brief This class represents a contact and it's behavior. 
*/
class Contact extends Person
{

	//! $sJobTitle holds the Contact job tiltle..
	public $sJobTitle;

	//! $sJobComapany holds the Contact company name..
	public $sJobComapany;

	//! $sCompanyDepartment holds the Contact company Department..
	public $sCompanyDepartment;

	//! $sCompanyLocation holds the Contact company Location..
	public $sCompanyLocation;

	/*//! $sImage holds the Contact Image..
	public $sImage;*/

	//! $sEmail holds the email id's..
	public $aEmail = array();

	//! $sEmail holds the email id's2..
	public $sEmail2;

	//! $aWorkPhone holds the works mobile phone..
	public $aWorkPhone = array();

	//! $aMobilePhone holds the mobile nos..
	public $aMobilePhone = array();

	//! $aAddress holds the Contact Addresses..
	public $aAddress = array();

	//! $aAddress holds the Contact Addresse2..
	public $sAddress2;

	//! $sNotes holds the any comment/message releted to contact..
	public $sNotes = array();

	//! $sBirthDay holds the contact date of birth..
	public $sBirthDay;

	//! $aUrl holds the contact personal websites..
	//!eg- like personal web site www.abc.com
	public $aUrl = array();

	public $sUrl2;

	//! $aRelationship holds the contact relationship to others..
	//! like child, mother, father, friend, spouse,partner,sister,brother etc
	public $aRelationship = array();

	//! $aSocialMessaging holds the Messenger no..
	//! E.g Facebook, Tweeter, 'Skype' Whatsapp, Line, Hike, etc etc.
	//! E.g $aSocialMessengers[] = array('Whatsapp'=>'9547855455','Facebook'=>"www.facebook.com/ambhoreShirish");
	public $aSocialMessaging = array();

	//! $aInternetCall holds the Internet call..
	//! E.g home,work,custom etc etc.
	public $aInternetCall = array();

	//! $sCustom holds the custom information of contact..
	//! E.g anything which is not in above properties.
	public $sCustom;

	//! $sFaxNo holds the fax no..
	public $aFaxNo = array();

	//! $sFaxNo holds the fax no..
	public $sFaxNo2;
	



	/* @brief initialize the contact class
	** @param $aContactData array which hold contact information.
	*/
	function __construct($aContactData)
	{	
		
		//! check $aContactDate array is empty or not..
		if(!empty($aContactData))
		{	
			//! check contact job title is empty or not..	
			if(!empty($aContactData['jobTitle'])){
				$this->sJobTitle = $aContactData['jobTitle'];
			}else{
				$this->sJobTitle = NULL;
			}

			//! check contact job company is empty or not..	
			if(!empty($aContactData['jobCompany'])){
				$this->sJobComapany = $aContactData['jobCompany'];
			}else{
				$this->sJobComapany = NULL;
			}

			
			//! check contact email is empty or not..	
			if(!empty($aContactData['email'])){
				$this->aEmail = $aContactData['email'];
			}else{
				$this->aEmail = NULL;
			}

			//! check contact work phone is empty or not..	
			if(!empty($aContactData['workPhone'])){
				$this->aWorkPhone = $aContactData['workPhone'];
			}else{
				$this->aWorkPhone = NULL;
			}

			//! check contact mobile phone is empty or not..	
			if(!empty($aContactData['mobilePhone'])){
				$this->aMobilePhone = $aContactData['mobilePhone'];
			}else{
				$this->aMobilePhone = NULL;
			}

			//! check address is empty or not..	
			if(!empty($aContactData['address'])){
				$this->aAddress = $aContactData['address'];
			}else{
				//$this->sAddress = NULL;
				echo "Contact Address not contains value.";
			}

			//! check contact notes is empty or not..	
			if(!empty($aContactData['notes'])){
				$this->sNotes = $aContactData['notes'];
			}else{
				$this->sNotes = NULL;
			}

			//! check contact birthday is empty or not..	
			if(!empty($aContactData['birthday'])){
				$this->sBirthDay = $aContactData['birthday'];
			}else{
				$this->sBirthDay = NULL;
			}

			//! check contact url is empty or not..	
			if(!empty($aContactData['url'])){
				$this->aUrl = $aContactData['url'];
			}else{
				$this->aUrl = NULL;
			}

			//! check contact url is empty or not..	
			if(!empty($aContactData['relationship'])){
				$this->aRelationship = $aContactData['relationship'];
			}else{
				$this->aRelationship = NULL;
			}

			//! check contact social Messaging is empty or not..	
			if(!empty($aContactData['socialMessaging'])){
				$this->aSocialMessaging = $aContactData['socialMessaging'];
			}else{
				$this->aSocialMessaging = NULL;
			}
			
			//! check contact Internet call is empty or not..	
			if(!empty($aContactData['internetCall'])){
				$this->aInternetCall = $aContactData['internetCall'];
			}else{
				$this->aInternetCall = NULL;
			}

			//! check contact custom is empty or not..	
			if(!empty($aContactData['custom'])){
				$this->sCustom = $aContactData['custom'];
			}else{
				$this->sCustom = NULL;
			}

			//! check contact fax is empty or not..	
			if(!empty($aContactData['fax'])){
				$this->aFaxNo = $aContactData['fax'];
			}else{
				$this->aFaxNo = NULL;
			}

			//! check contact fax is empty or not..	
			if(!empty($aContactData['firstName'])){
				$this->sFirstName = $aContactData['firstName'];
			}else{
				$this->sFirstName = NULL;
			}

			//! check contact fax is empty or not..	
			if(!empty($aContactData['lastName'])){
				$this->sLastName = $aContactData['lastName'];
			}else{
				$this->sLastName = NULL;
			}

			//! check contact fax is empty or not..	
			if(!empty($aContactData['nickName'])){
				$this->sNickName = $aContactData['nickName'];
			}else{
				$this->sNickName = NULL;
			}

			//! check company department is empty or not..	
			if(!empty($aContactData['companyDepartment'])){
				$this->sCompanyDepartment = $aContactData['companyDepartment'];
			}else{
				$this->sCompanyDepartment = NULL;
			}

			//! check company Location is empty or not..	
			if(!empty($aContactData['companyLocation'])){
				$this->sCompanyLocation = $aContactData['companyLocation'];
			}else{
				$this->sCompanyLocation = NULL;
			}

			//! check Email2 is empty or not..	
			if(!empty($aContactData['email2'])){
				$this->sEmail2 = $aContactData['email2'];
			}else{
				$this->sEmail2 = NULL;
			}

			//! check Address2 is empty or not..	
			if(!empty($aContactData['address2'])){
				$this->sAddress2 = $aContactData['address2'];
			}else{
				$this->sAddress2 = NULL;
			}

			//! check Url2 is empty or not..	
			if(!empty($aContactData['url2'])){
				$this->sUrl2 = $aContactData['url2'];
			}else{
				$this->sUrl2 = NULL;
			}

			//! check Fax2 is empty or not..	
			if(!empty($aContactData['fax2'])){
				$this->sFaxNo2 = $aContactData['fax2'];
			}else{
				$this->sFaxNo2 = NULL;
			}

			//! $aPerson array contains person details..
				$aPerson = array(	
					'name'=> $aContactData['contactName'],
					'gender'=>$aContactData['gender'],
					'age'=>$aContactData['age'],
					'DOB'=>$aContactData['DOB'],					
					'firstName'=>$aContactData['firstName'],
					'lastName'=>$aContactData['lastName'],
					'nickName'=>$aContactData['nickName'],
			
				);
				//! call parent class constructor...
				 parent::__construct($aPerson);		
		}
		else{
			return false;
		}
	}

	/*! @brief adds the Contact
    *  Calling this function will create a new Contact. Once we have set the Contact details, we can call this function and add its entry in the database.
    * @return Integer It will return insertion id if Contact is added successfully. On failure, it will return fale.
    */
    function addContact() {
        $sessionManager = new SessionManager();
        $DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();
        
        $sTableName = "contact";
               
        $sQuery = "INSERT INTO `contact`(`contactId`, `id_fs_name`, `id_fs_firstName`, `id_fs_lastName`, `id_fs_nickName`, `id_fs_gender`, `id_fs_jobTitle`, `id_fs_jobComapany`, `id_fs_companyDepartment`, `id_fs_companyLocation`, `id_fs_email1`, `id_fs_email2`, `id_fs_workPhone`, `id_fs_mobilePhone`, `id_fs_address1`, `id_fs_street1`, `id_fs_city1`, `id_fs_postalCode1`, `id_fs_country1`, `id_fs_address2`, `id_fs_street2`, `id_fs_city2`, `id_fs_postalCode2`, `id_fs_country2`, `id_fs_birthDay`, `id_fs_url1`, `id_fs_url2`, `id_fs_relationship`, `id_fs_socialMessaging`, `id_fs_internetCall`, `id_fs_custom`, `id_fs_faxNo1`, `id_fs_faxNo2`, `id_fs_hobby`, `id_fs_subject`, `id_fs_userId`, `deleted`) VALUES 
        (Null,'{$this->sName}','{$this->sFirstName}','{$this->sLastName}','{$this->sNickName}','{$this->sGender}','{$this->sJobTitle}','{$this->sJobComapany}','{$this->sCompanyDepartment}','{$this->sCompanyLocation}','{$this->aEmail}','{$this->sEmail2}','{$this->aWorkPhone}','{$this->aMobilePhone}','{$this->aAddress}',0,0,0,0,'{$this->sAddress2}',0,0,0,0,'{$this->sBirthDay}','{$this->aUrl}','{$this->sUrl2}','{$this->aRelationship}','{$this->aSocialMessaging}','{$this->aInternetCall}','{$this->sCustom}','{$this->aFaxNo}','{$this->sFaxNo2}',0,0,'{$this->iUserId}',0)";
        $rResult = $conn->query($sQuery);

        if($rResult) {
            $this->iContactId = $conn->insert_id;               	
		        
            return $this->iContactId;
        }
        else {            
            return false;
        }
    }



    /*! @brief get the Contact details..  
    * @param $iContactId
    * @return $aContact as array...
    */
    public function fGetContactDetails($iContactId) {
        
        $DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();
        
        $sTableName = "contact";

        $sQuery = "SELECT * FROM {$sTableName} WHERE `contactId` = {$iContactId}  and `deleted` = 0";
        
        $result = $conn->query($sQuery);

		$aContact = array();    
		if($result!==FALSE){

			while($row = $result->fetch_array()) {
				$aContact = $row;
			}

		}
		else{		
			return false;
		}
		return $aContact;
    }

    /*! @brief get the User Contacts..  
    * @param $iUserId
    * @return $aContactData as array...
    */
    public function getContacts($iUserId) {
        
        $DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();
        
        $sTableName = "contact";

        $sQuery = "SELECT * FROM {$sTableName} WHERE `id_fs_userId` = '{$iUserId}' and `deleted` =0";
        
        $result = $conn->query($sQuery);

		$aContactData = array();    
		if($result!==FALSE){

			while($row = $result->fetch_array()) {
				$aContactData[] = $row;
			}

		}
		else{		
			return false;
		}
		return $aContactData;
    }



    /*! @brief Update the Contact details
    *  Calling this function will create a new Contact. Once we have set the Contact details, we can call this function and add its entry in the database.
    * @return Integer It will return insertion id if Contact is added successfully. On failure, it will return fale.
    */
    function fUpdateContact($iContactId){
        $sessionManager = new SessionManager();

        $DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();
        
        $sTableName = "contact";

        $sQuery = "UPDATE {$sTableName} SET `deleted`= 1 WHERE `contactId` ={$iContactId}";
		$rResult = $conn->query($sQuery);
		if ($rResult) {   			
			$sQuery1 = "INSERT INTO `contact`(`contactId`, `id_fs_name`, `id_fs_firstName`, `id_fs_lastName`, `id_fs_nickName`, `id_fs_gender`, `id_fs_jobTitle`, `id_fs_jobComapany`, `id_fs_companyDepartment`, `id_fs_companyLocation`, `id_fs_email1`, `id_fs_email2`, `id_fs_workPhone`, `id_fs_mobilePhone`, `id_fs_address1`, `id_fs_street1`, `id_fs_city1`, `id_fs_postalCode1`, `id_fs_country1`, `id_fs_address2`, `id_fs_street2`, `id_fs_city2`, `id_fs_postalCode2`, `id_fs_country2`, `id_fs_birthDay`, `id_fs_url1`, `id_fs_url2`, `id_fs_relationship`, `id_fs_socialMessaging`, `id_fs_internetCall`, `id_fs_custom`, `id_fs_faxNo1`, `id_fs_faxNo2`, `id_fs_hobby`, `id_fs_subject`, `id_fs_userId`, `deleted`) VALUES 
        	(Null,'{$this->sName}','{$this->sFirstName}','{$this->sLastName}','{$this->sNickName}','{$this->sGender}','{$this->sJobTitle}','{$this->sJobComapany}','{$this->sCompanyDepartment}','{$this->sCompanyLocation}','{$this->aEmail}','{$this->sEmail2}','{$this->aWorkPhone}','{$this->aMobilePhone}','{$this->aAddress}',0,0,0,0,'{$this->sAddress2}',0,0,0,0,'{$this->sBirthDay}','{$this->aUrl}','{$this->sUrl2}','{$this->aRelationship}','{$this->aSocialMessaging}','{$this->aInternetCall}','{$this->sCustom}','{$this->aFaxNo}','{$this->sFaxNo2}',0,0,'{$this->iUserId}',0)";
			$rResult1 = $conn->query($sQuery1);
	        if($rResult1) {
	            $iContactId = $conn->insert_id;	
	            return $iContactId;
	        }
	        else {            
	            return false;
	        }
		}
	}    


    
    /*! @brief get the all Contact from database  
    * @return $aContact as array...
    */
    public function fGetAllContact() {
        
        $DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();
        
        $sTableName = "contact";

        $sQuery = "SELECT * FROM {$sTableName} WHERE `deleted`=0";
        
        $result = $conn->query($sQuery);

		$aContact = array();    
		if($result!==FALSE){

			while($row = $result->fetch_array()) {
				$aContact[] = $row;
			}

		}
		else{		
			return false;
		}
		return $aContact;
    }

    /*! @brief adds the notes
    *  Calling this function will add new notes. Once we have set the note details, we can call this function and add its entry in the database.
    */
    function addNotes($aNote,$iContactId) {
        
        $DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();
        
        $sTableName = "notes";
         
        foreach ($aNote as $key => $aNote) { 
	        $sQuery = "INSERT INTO {$sTableName} (`noteId`, `contactId`, `notes`, `deleted`) VALUES (Null,'{$iContactId}','{$aNote['id_fs_notes']}',0)";
	        //var_dump($sQuery);
	        $rResult = $conn->query($sQuery);

	        if($rResult) {
	            $this->iContactId = $conn->insert_id;         
	            //return $this->iContactId;
	        }
	        else {            
	            return false;
	        }
	    }
    }

    /*! @brief get the note details  
    * @param $iContactId
    * @return $aNoteData as array...
    */
    public function fGetNote($iContactId) {
        
        $DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();
        
        $sTableName = "notes";

        $sQuery = "SELECT * FROM {$sTableName} WHERE `contactId` = {$iContactId} and `deleted`=0 ";
        
        $result = $conn->query($sQuery);

		$aNoteData = array();    
		if($result!==FALSE){

			while($row = $result->fetch_array()) {
				$aNoteData[] = $row;
			}
		}
		else{		
			return false;
		}
		return $aNoteData;
    }


    /*! @brief Update the notes details
    *  Calling this function will add new note and set deleted = 1 flag on previous id.
    */
    function updateNotes($aNotes,$iContactId){
        
        $DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();
        
        $sTableName = "notes";

        $sQuery = "UPDATE {$sTableName} SET `deleted`= 1 WHERE `contactId` ={$iContactId}";
		$rResult = $conn->query($sQuery);

		if ($rResult) {               
	        foreach ($aNotes as $key => $aNote) { 
		        $sQuery = "INSERT INTO {$sTableName} (`noteId`, `contactId`, `notes`, `deleted`) VALUES (Null,'{$iContactId}','{$aNote['id_fs_notes']}',0)";
		        //var_dump($sQuery);
		        $rResult = $conn->query($sQuery);

		        if($rResult) {
		            $this->iContactId = $conn->insert_id;         
		            //return $this->iContactId;
		        }
		        else {            
		            return false;
		        }
		    }
	    }
    }


    /*! @brief adds the Admin contacts mapping
    *  Calling this function will add Admin contacts. Once we have set the AdminId and contactId, we can call this function and add its entry in the database.
    */
    function addAdminContactMapping($iContactId,$iUserID){
        
        $DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();
        
        $sTableName = "admincontactmapping";
        $sQuery = "INSERT INTO {$sTableName} (`ACMappingId`, `adminId`, `contactId`, `deleted`) VALUES (null,'{$iUserID}','{$iContactId}',0)";
        $rResult = $conn->query($sQuery);

        if($rResult) {
            $this->iAdminContactId = $conn->insert_id;         
            //return $this->iAdminContactId;
        }
        else {            
            return false;
        }
    }


     /*! @brief delete the admin contacts  
    * @return $true if successfully deleted otherewise return false..
    */
    function deleteAdminContact() {

		$iContactId = $_POST['contactId'];
		$DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();
		
		$sTable = 'contact';		

		$sQuery = "UPDATE {$sTable} SET deleted=1 WHERE contactId={$iContactId} AND deleted = 0";    
		$rResult = $conn->query($sQuery);
		if($rResult){
			$sQuery = "UPDATE `admincontactmapping` SET deleted=1 WHERE contactId={$iContactId} AND deleted = 0";    
			$rResult = $conn->query($sQuery);
			echo 1;
		//return true;	
		}
		else{		
			return false;
		}	
	} 


	/*! @brief delete the Employee contacts.. 
    * @return $true if successfully deleted otherewise return false..
    */
    function deleteEmpContact() {

		$iContactId = $_POST['contactId'];
		$DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();
		
		$sTable = 'contact';		

		$sQuery = "UPDATE {$sTable} SET deleted=1 WHERE contactId={$iContactId} AND deleted = 0";    
		$rResult = $conn->query($sQuery);
		if($rResult){
			$sQuery = "UPDATE `empcontactmapping` SET deleted=1 WHERE contactId={$iContactId} AND deleted = 0";    
			$rResult = $conn->query($sQuery);
			echo 1;
		//return true;	
		}
		else{		
			return false;
		}	
	}



	 /*! @brief insert the bulk contact data
	 * @param $aContactData and $iUserId..  
	 * @FFU...
    * @return $true if successfully inserted otherewise return false..
    */
    function addBulkContact($aContactData,$iUserId) {

		$DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();
		
		$sTable = 'bulkcontact';		

		$sQuery = "INSERT INTO {$sTable} (`builkContactId`, `userId`, `data`, `deleted`) VALUES (NULL,'{$iUserId}','{$aContactData}',0)";    
		$rResult = $conn->query($sQuery);
		if($rResult){
			 $id = $conn->insert_id;
			 return $id; 
		}
		else{		
			return false;
		}	
	} 

	/*! @brief check the Admin contact is already in database or not
	* @param $sName,$sEmail,$iAdminId  
	* @FFU...
    * @return 0 if there is no entry, 1 if yes..
    */
	function checkContactExist($sName,$sEmail,$iUserId){
		$DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();

		$sQuery = "SELECT `id_fs_name`, `id_fs_email1` FROM `contact` WHERE `deleted` = 0 and `id_fs_name` = '{$sName}' and `id_fs_email1` = '{$sEmail}'  and `id_fs_userId` = '{$iUserId}' ";
		$result = $conn->query($sQuery);

		//var_dump($sQuery);exit();
		$row = $result->fetch_array();
		
		$sExistName = $row["id_fs_name"];
		$sExistEmail = $row['id_fs_email1'];
		if($sExistName=="" && $sExistEmail=="") {
			return 0;
		}
		else{
			return 1;
		}
	}





	/* 
	** Destructor for Destroy the class.
	*/
	function __destruct()
		{
			//echo "Class Destroyed";
		}
}

//! for check the value of f is set and the value of f is deleteAdminContact
//! then call deleteAdminContact()....  
	if (isset($_GET['f']) && $_GET['f']="deleteAdminContact"){
	  $objContact = new Contact();
	  $objContact->deleteAdminContact();
	}

//! for check the value of f is set and the value of f is deleteEmpContact
//! then call deleteEmpContact().... 

	if (isset($_GET['f']) && $_GET['f']="deleteEmpContact"){
	  $objContact = new Contact();
	  $objContact->deleteEmpContact();
	}
?>