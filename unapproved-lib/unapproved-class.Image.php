<?php
include_once "class.DBConnManager.php";
/**
* @brief  This class represents a image and it's behavior.
*/
class Image
{
	
	//! $sImageName holds the image name..
	public $sImageName;
	
	//! $sImageName holds the complete path to the image..
	public $sImagePath;
	
	//! $sImageType holds the image Type..
	public $sImageType;
	
	//! $aImageformat holds the image Format..
	public $aImageformat;
	
	//! $sImagesize holds the image Size..
	public $sImagesize;
	
	//! $sImageResoluation holds the image Resoluation..
	public $sImageResoluation;
	
	//! $sImageHeight holds the image Height..
	public $sImageHeight;
	
	//! $sImageWidht holds the image width..
	public $sImageWidth;

	

	/* @brief initialize the Image class
	** @param $aImageData array which hold image information.
	*/
	function __construct($aImageData)
	{
		if(!empty($aImageData))
		{
			if(!empty($aImageData['imageName']))
			{
				$this->sImageName= $aImageData['imageName'];
			}else
			{
				$this->sImageName = NULL;
			}

			if(!empty($aImageData['tmp_name']))
			{
				$this->sImagePath= $aImageData['tmp_name'];
			}else
			{
				$this->sImagePath = NULL;
			}

			if(!empty($aImageData['imageType']))
			{
				$this->sImageType= $aImageData['imageType'];
			}else
			{
				$this->sImageType = NULL;
			}
			if(!empty($aImageData['imageformat']))
			{
				$this->aImageformat= $aImageData['imageformat'];
			}else
			{
				$this->aImageformat = NULL;
			}
			if(!empty($aImageData['imagesize']))
			{
				$this->sImagesize= $aImageData['imagesize'];
			}else
			{
				$this->sImagesize = NULL;
			}
			if(!empty($aImageData['imageResoluation']))
			{
				$this->sImageResoluation= $aImageData['imageResoluation'];
			}else
			{
				$this->sImageResoluation = NULL;
			}
			if(!empty($aImageData['imageHeight']))
			{
				$this->sImageHeight= $aImageData['imageHeight'];
			}else
			{
				$this->sImageHeight = NULL;
			}
			if(!empty($aImageData['imageWidth']))
			{
				$this->sImageWidth= $aImageData['imageWidth'];
			}else
			{
				$this->sImageWidth = NULL;
			}
			
			
		}
		else			
		{
			return false;
		}	


	}
	/*! @brief adds the Image
    *  Calling this function will create a add Image details in database.
    * @return Integer It will return insertion id if image details is added successfully. On failure, it will return fasle.
    */
    function addImage(){	

	$sExtension = array_pop(explode('.',basename($this->sImageName)));
	$sSysFileName = md5(uniqid('',true)).".".$sExtension;
	$sSaveFileTo =  "Images/". $sSysFileName;
	$sTempName = $this->sImagePath;
	//! Move to permanent location and check if it can be moved
		if (move_uploaded_file($this->sImagePath, $sSaveFileTo)) {
		//var_dump($this->sImagePath);

		//!for getting database connection....
			$DBMan = new DBConnManager();
			$conn =  $DBMan->getConnInstance();
	        
	        $sTableName = "image";
	        //! Query for inserting image in database..
	        $sQuery = "INSERT INTO {$sTableName} (`imageId`, `id_fs_imageName`, `id_fs_imageType`, `id_fs_imageformat`, `id_fs_imagesize`, `id_fs_imageResoluation`, `id_fs_imageHeight`, `id_fs_imageWidth`,`id_fs_extension`, `id_fs_path`, `id_fs_systemName`, `id_fs_tempName`, `deleted`) VALUES (null,'{$this->sImageName}','{$this->sImageType}','{$this->aImageformat}','{$this->sImagesize}','{$this->sImageResoluation}','{$this->sImageHeight}','{$this->sImageWidth}','{$sExtension}','{$sSaveFileTo}','{$sSysFileName}','{$sTempName}',0)";
	        //var_dump($sQuery);
	        $rResult = $conn->query($sQuery);

	        if($rResult) {
	            $this->iImageId = $conn->insert_id;

	            return $this->iImageId;
	        }
	        else {            
	            return false;
	        }
	    }
	}


	/*! @brief adds the Image Conatact Mapping..
    *  Calling this function will add Image Contact ids database.
    * @return iMappId
    */
    function addContactImageMapping($iImageId,$iContactId){	

		$DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();
        
        $sTableName = "contactimagemapping";
        $sQuery = "INSERT INTO {$sTableName} (`CIMappingId`, `imageId`, `contactId`, `deleted`) VALUES (Null,'{$iImageId}','{$iContactId}',0)";
        
        $rResult = $conn->query($sQuery);

        if($rResult) {
            $iMappId = $conn->insert_id;
            return $iMappId;
        }
        else {            
            return false;
        }
	   
	}


	/*! @brief  get Image id 
	*   @param $iImageId int contains the unique id file
	*   @return $aData return array contain file details
	*/
	function getImage($iImageId) {
		//!for getting database connection....
		$DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();
		
		$sTableName = 'image';

		$sQuery = "SELECT * FROM `{$sTableName}` WHERE `imageId`={$iImageId} AND deleted=0";
		$rResult = $conn->query($sQuery);
		$aData = array();
		if($rResult){
			while($aRow = $rResult->fetch_array()) {
				$aData = $aRow;
			}
			return $aData;
		}
		else {			
			return false;
		}
	}


	/*! @brief  get Image id 
	*   @param $iContactId int contains the contact id Contact
	*   @return $aData return array contain file details
	*/
	function getImageIdByContactId($iContactId) {
		//!for getting database connection....
		$DBMan = new DBConnManager();
		$conn =  $DBMan->getConnInstance();
		
		$sTableName = 'contactimagemapping';

		$sQuery = "SELECT `imageId` FROM `{$sTableName}` WHERE `contactId`={$iContactId} AND deleted=0";
		$rResult = $conn->query($sQuery);
		$iImageId = array();
		if($rResult){
			while($aRow = $rResult->fetch_array()) {
				$iImageId = $aRow[0];
			}
			return $iImageId;
		}
		else {			
			return false;
		}
	}
	


	/* 
	** for destruct the Organisation class.
	*/
	function __destruct()
		{
			//echo "Class Destroyed";
		}	
}
?>