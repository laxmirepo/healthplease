<?php
include('base-header.php');
include('page-header.php');
?>
	

<div class="classTopHeading">
        <div class="container">
            <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Welcome Admin</h1>
            </div>
        </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>
        <div id="page-wrappera" class="container">
            
            <!-- /.row -->
            <!-- <div class="row">
                <form action="" class="classFormAddLocation" id="idAddLocation" method="post"> 
                    <div class="col-lg-6 col-lg-offset-4">
                         <input type="text" name="addLocation" id="idAddLocation" class="form-control form-group classAddLocation">
                    </div>  
                      
                    <div class="col-lg-2">
                        <input type="button" class="btn btn-md btn-primary classAddButton" id="idAddLocationBtn" value="Add Location">
                    </div>
                </form>    
            </div> -->

            <div class="row">
                <div class="col-lg-9 col-lg-offset-3">
                    <section>
                    <table class="table table-bordered " id="idLocationTable" >
                            <thead>
                                <tr>
                                    <!-- <th>Sr.No</th> -->
                                    <th>Location</th>
                                    <!-- <th>Status</th> -->
                                </tr>
                            </thead>
                            <tbody id="idTableLocation">
                            </tbody>
                    </table>
                   </section> 
            <input tpye="hidden" name="created_by" value="1">       
            <input tpye="hidden" name="updated_by" value="1">
            <input type="hidden" name="is_valid" value="1">
            <?php $dateTime = date("Y-m-d H:i:s");?>
            <input type="hidden" name="created_date" value="<?php echo $dateTime; ?>">
            <input type="hidden" name="updated_date" value="<?php echo $dateTime; ?>">
<!-- <table id="example" class="display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>

						<th>First name</th>
						<th>Last name</th>
						<th>Position</th>
						<th>Office</th>
						<th width="18%">Start date</th>
						<th>Salary</th>
					</tr>
				</thead>
			</table> -->
                </div>                        
            </div> 
          
        </div>
        <!-- /#page-wrapper -->




<script type="text/javascript">


var editor; // use a global for the submit and return data rendering in the examples

$(document).ready(function() {
	editor = new $.fn.dataTable.Editor( {
		ajax: "ajax_location.php",
		table: "#idLocationTable",
		fields: [ {
				label: "location_name:",
				name: "location_name"
			}
		]
	} );

	// Activate an inline edit on click of a table cell
	$('#idLocationTable').on( 'click', 'tbody td:not(:first-child)', function (e) {
		editor.inline( this );
	} );

	$('#idLocationTable').DataTable( {
		dom: "Tfrtip",
		ajax: "ajax_location.php",
		columns: [
			{ data: null, defaultContent: '', orderable: false },
            { data: "created_by"},
            { data: "updated_by"},
            { data: "is_valid"},
            { data: "created_date"},
            { data: "updated_date"},
			{ data: "location_name"}
		],
		order: [ 1, 'asc' ],
		tableTools: {
			sRowSelect: "os",
			sRowSelector: 'td:first-child',
			aButtons: [
				{ sExtends: "editor_create", editor: editor },
				{ sExtends: "editor_edit",   editor: editor },
				{ sExtends: "editor_remove", editor: editor }
			]
		}
	} );
} );



	</script>



<?php
include('page-footer.php');
include('base-footer.php');
?>